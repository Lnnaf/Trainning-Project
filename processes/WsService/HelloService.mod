[Ivy]
17886931DD79B8AB 7.5.0 #module
>Proto >Proto Collection #zClass
He0 HelloService Big #zClass
He0 WS #cInfo
He0 #process
He0 @TextInP .webServiceName .webServiceName #zField
He0 @TextInP .implementationClassName .implementationClassName #zField
He0 @TextInP .authenticationType .authenticationType #zField
He0 @AnnotationInP-0n ai ai #zField
He0 @TextInP .type .type #zField
He0 @TextInP .processKind .processKind #zField
He0 @TextInP .xml .xml #zField
He0 @TextInP .responsibility .responsibility #zField
He0 @StartWS f0 '' #zField
He0 @EndWS f1 '' #zField
He0 @GridStep f2 '' #zField
He0 @PushWFArc f3 '' #zField
He0 @PushWFArc f4 '' #zField
>Proto He0 He0 HelloService #zField
He0 f0 inParamDecl '<String name> param;' #txt
He0 f0 inParamTable 'out.name=param.name;
' #txt
He0 f0 outParamDecl '<> result;' #txt
He0 f0 callSignature call(String) #txt
He0 f0 useUserDefinedException false #txt
He0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(String)</name>
    </language>
</elementInfo>
' #txt
He0 f0 @C|.responsibility Everybody #txt
He0 f0 81 49 30 30 -13 17 #rect
He0 f0 @|StartWSIcon #fIcon
He0 f1 337 49 30 30 0 15 #rect
He0 f1 @|EndWSIcon #fIcon
He0 f2 actionTable 'out=in;
' #txt
He0 f2 actionCode 'out.name = "Hello "+ in.name;' #txt
He0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
He0 f2 160 58 112 44 0 -8 #rect
He0 f2 @|StepIcon #fIcon
He0 f3 110 65 160 80 #arcP
He0 f4 272 80 337 65 #arcP
>Proto He0 .webServiceName app.HelloService #txt
>Proto He0 .type WsService.HelloServiceData #txt
>Proto He0 .processKind WEB_SERVICE #txt
>Proto He0 -8 -8 16 16 16 26 #rect
>Proto He0 '' #fIcon
He0 f0 mainOut f3 tail #connect
He0 f3 head f2 mainIn #connect
He0 f2 mainOut f4 tail #connect
He0 f4 head f1 mainIn #connect
