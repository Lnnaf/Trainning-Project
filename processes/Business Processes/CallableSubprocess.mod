[Ivy]
1789185E3A9F5D27 7.5.0 #module
>Proto >Proto Collection #zClass
Cs0 CallableSubprocess Big #zClass
Cs0 B #cInfo
Cs0 #process
Cs0 @AnnotationInP-0n ai ai #zField
Cs0 @TextInP .type .type #zField
Cs0 @TextInP .processKind .processKind #zField
Cs0 @TextInP .xml .xml #zField
Cs0 @TextInP .responsibility .responsibility #zField
Cs0 @StartSub f0 '' #zField
Cs0 @EndSub f1 '' #zField
Cs0 @PushWFArc f2 '' #zField
>Proto Cs0 Cs0 CallableSubprocess #zField
Cs0 f0 inParamDecl '<> param;' #txt
Cs0 f0 outParamDecl '<> result;' #txt
Cs0 f0 callSignature call() #txt
Cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call()</name>
    </language>
</elementInfo>
' #txt
Cs0 f0 81 49 30 30 -13 17 #rect
Cs0 f0 @|StartSubIcon #fIcon
Cs0 f1 337 49 30 30 0 15 #rect
Cs0 f1 @|EndSubIcon #fIcon
Cs0 f2 111 64 337 64 #arcP
>Proto Cs0 .type app.CallableSubprocessData #txt
>Proto Cs0 .processKind CALLABLE_SUB #txt
>Proto Cs0 0 0 32 24 18 0 #rect
>Proto Cs0 @|BIcon #fIcon
Cs0 f0 mainOut f2 tail #connect
Cs0 f2 head f1 mainIn #connect
