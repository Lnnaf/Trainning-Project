[Ivy]
17880FB513A3421D 7.5.0 #module
>Proto >Proto Collection #zClass
Ss0 SubProcess Big #zClass
Ss0 B #cInfo
Ss0 #process
Ct0 Component Big #zClass
Ct0 B #cInfo
Ss0 @AnnotationInP-0n ai ai #zField
Ss0 @TextInP .type .type #zField
Ss0 @TextInP .processKind .processKind #zField
Ss0 @TextInP .xml .xml #zField
Ss0 @TextInP .responsibility .responsibility #zField
Ss0 @StartRequest f0 '' #zField
Ss0 @EndTask f1 '' #zField
Ss0 @UserDialog f3 '' #zField
Ss0 Ct0 S10 'Sub 1' #zField
Ss0 @GridStep f6 '' #zField
Ss0 @PushWFArc f2 '' #zField
Ss0 @GridStep f8 '' #zField
Ss0 @PushWFArc f7 '' #zField
Ss0 @PushWFArc f4 '' #zField
Ss0 @PushWFArc f5 '' #zField
>Proto Ss0 Ss0 SubProcess #zField
Ct0 @AnnotationInP-0n ai ai #zField
Ct0 @TextInP .type .type #zField
Ct0 @TextInP .processKind .processKind #zField
Ct0 @TextInP .xml .xml #zField
Ct0 @TextInP .responsibility .responsibility #zField
Ct0 @PushTrueWFInG-01 g0 '' #zField
Ct0 @PushTrueWFOutG-01 g1 '' #zField
Ct0 @GridStep f1 '' #zField
Ct0 @PushWFArc f0 '' #zField
>Proto Ct0 Ct0 Component #zField
Ss0 f0 outLink start.ivp #txt
Ss0 f0 inParamDecl '<> param;' #txt
Ss0 f0 requestEnabled true #txt
Ss0 f0 triggerEnabled false #txt
Ss0 f0 callSignature start() #txt
Ss0 f0 caseData businessCase.attach=true #txt
Ss0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Ss0 f0 @C|.responsibility Everybody #txt
Ss0 f0 81 49 30 30 -21 17 #rect
Ss0 f0 @|StartRequestIcon #fIcon
Ss0 f1 849 49 30 30 0 15 #rect
Ss0 f1 @|EndIcon #fIcon
Ss0 f3 dialogId app.SubProcess #txt
Ss0 f3 startMethod start() #txt
Ss0 f3 requestActionDecl '<> param;' #txt
Ss0 f3 responseMappingAction 'out.employee=result.employee;
' #txt
Ss0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>SubProcess</name>
    </language>
</elementInfo>
' #txt
Ss0 f3 240 130 112 44 -34 -8 #rect
Ss0 f3 @|UserDialogIcon #fIcon
Ss0 S10 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language lang="en">
        <name>Sub 1</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ss0 S10 496 42 112 44 -16 -8 #rect
Ss0 S10 @|BIcon #fIcon
Ss0 S10 g0 -54 8 #fFoot
Ss0 f6 actionTable 'out=in;
' #txt
Ss0 f6 actionCode 'ivy.log.info("======[base process]===="+in.employee.name);' #txt
Ss0 f6 656 42 112 44 0 -8 #rect
Ss0 f6 @|StepIcon #fIcon
Ss0 f2 768 64 849 64 #arcP
Ss0 f8 actionTable 'out=in;
' #txt
Ss0 f8 104 154 112 44 0 -8 #rect
Ss0 f8 @|StepIcon #fIcon
Ss0 f7 608 64 656 64 #arcP
Ss0 f4 109 70 246 130 #arcP
Ss0 f5 352 130 498 72 #arcP
>Proto Ss0 .type app.SubProcessData #txt
>Proto Ss0 .processKind NORMAL #txt
>Proto Ss0 0 0 32 24 18 0 #rect
>Proto Ss0 @|BIcon #fIcon
Ct0 g0 -2 336 26 26 0 5 #rect
Ct0 g0 @|MIGIcon #fIcon
Ct0 g1 563 254 26 26 0 5 #rect
Ct0 g1 @|MOGIcon #fIcon
Ct0 f1 actionTable 'out=in;
' #txt
Ct0 f1 actionCode '
import ch.ivyteam.ivy.workflow.ITask;
import ch.ivyteam.ivy.workflow.ICase;
import app.CaseUtils;

ivy.log.info("==[sub process]====="+out.employee.name);

CaseUtils caseUtils;
ivy.log.info(caseUtils.findcase(1));
ICase case = caseUtils.findcase(1);
ivy.log.info(case.customFields().stringField("customField1").getOrNull());' #txt
Ct0 f1 240 245 112 44 0 -8 #rect
Ct0 f1 @|StepIcon #fIcon
Ct0 f0 23 347 563 268 #arcP
Ct0 f0 0 0.4631211625202107 0 0 #arcLabel
>Proto Ct0 0 0 32 24 18 0 #rect
>Proto Ct0 @|BIcon #fIcon
Ss0 f6 mainOut f2 tail #connect
Ss0 f2 head f1 mainIn #connect
Ss0 S10 g1 f7 tail #connect
Ss0 f7 head f6 mainIn #connect
Ss0 f0 mainOut f4 tail #connect
Ss0 f4 head f3 mainIn #connect
Ss0 f3 mainOut f5 tail #connect
Ss0 f5 head S10 g0 #connect
Ct0 g0 m f0 tail #connect
Ct0 f0 head g1 m #connect
Ct0 0 0 640 512 0 #ivRect
