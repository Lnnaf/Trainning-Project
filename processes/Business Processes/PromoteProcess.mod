[Ivy]
1786884F5ABFBF80 7.5.0 #module
>Proto >Proto Collection #zClass
Ps0 PromoteProcess Big #zClass
Ps0 B #cInfo
Ps0 #process
Ps0 @AnnotationInP-0n ai ai #zField
Ps0 @TextInP .type .type #zField
Ps0 @TextInP .processKind .processKind #zField
Ps0 @TextInP .xml .xml #zField
Ps0 @TextInP .responsibility .responsibility #zField
Ps0 @StartRequest f0 '' #zField
Ps0 @EndTask f1 '' #zField
Ps0 @UserDialog f3 '' #zField
Ps0 @PushWFArc f4 '' #zField
Ps0 @UserTask f8 '' #zField
Ps0 @CallSub f2 '' #zField
Ps0 @PushWFArc f5 '' #zField
Ps0 @TkArc f9 '' #zField
Ps0 @PushWFArc f6 '' #zField
>Proto Ps0 Ps0 PromoteProcess #zField
Ps0 f0 outLink startPromote.ivp #txt
Ps0 f0 inParamDecl '<> param;' #txt
Ps0 f0 requestEnabled true #txt
Ps0 f0 triggerEnabled false #txt
Ps0 f0 callSignature startPromote() #txt
Ps0 f0 startName 'Promote Request' #txt
Ps0 f0 caseData 'businessCase.attach=true
customFields.STRING.CUSTOMIZATION_ADDITIONAL_CASE_DETAILS_PAGE="Start Processes/test234/start.ivp"' #txt
Ps0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>startPromote.ivp</name>
    </language>
</elementInfo>
' #txt
Ps0 f0 @C|.responsibility Everybody #txt
Ps0 f0 81 57 30 30 -21 17 #rect
Ps0 f0 @|StartRequestIcon #fIcon
Ps0 f1 81 561 30 30 0 15 #rect
Ps0 f1 @|EndIcon #fIcon
Ps0 f3 dialogId app.PromoteForm #txt
Ps0 f3 startMethod start() #txt
Ps0 f3 requestActionDecl '<> param;' #txt
Ps0 f3 responseMappingAction 'out=in;
out.employees=result.employees;
out.promoteForm=result.promoteForm;
out.promoteForm.employees=result.employees;
' #txt
Ps0 f3 responseActionCode 'import repository.PromoteFormRepository;
import entity.PromoteForm;
ivy.case.createNote(ivy.session, "Save form to Database");

import entity.Employee;
Employee emp = new Employee();


PromoteForm form = new PromoteForm();
form.employees = out.employees;
form = out.promoteForm;
form.caseId= ivy.case.getId().toString();

ivy.repo.save(form);' #txt
Ps0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Employee</name>
    </language>
</elementInfo>
' #txt
Ps0 f3 68 248 56 48 -27 -8 #rect
Ps0 f3 @|UserDialogIcon #fIcon
Ps0 f4 96 396 96 561 #arcP
Ps0 f8 dialogId app.PromoteForm #txt
Ps0 f8 startMethod startApproved(entity.PromoteForm,List<entity.Employee>) #txt
Ps0 f8 requestActionDecl '<entity.PromoteForm promoteForm,List<entity.Employee> employees> param;' #txt
Ps0 f8 requestMappingAction 'param.promoteForm=in.promoteForm;
param.employees=in.employees;
' #txt
Ps0 f8 responseMappingAction 'out=in;
' #txt
Ps0 f8 caseData 'customFields.STRING.CUSTOMIZATION_ADDITIONAL_CASE_DETAILS_PAGE="Start Processes/test234/start.ivp"' #txt
Ps0 f8 taskData 'TaskA.EXROL=ProjectManager
TaskA.EXTYPE=0
TaskA.NAM=<%\=in.promoteForm.creator%>''s request
TaskA.ROL=ProjectManager
TaskA.SCRIPT=import ch.ivyteam.ivy.security.IUser;\r\nimport ch.ivyteam.ivy.persistence.query.IPagedIterable;\r\nimport ch.ivyteam.ivy.workflow.ICase;\r\n\r\nimport ch.ivyteam.ivy.process.model.value.SignalCode;\r\n\r\n\r\nIPagedIterable<IUser> users \=  task.activator().userCandidatesPaged();\r\nfor(IUser user\:users){\r\nin.emails.add(user.eMailAddress);\r\n}\r\n\r\n\r\nivy.wf.signals().send(new SignalCode("save\:case"), in);\r\n\r\n\r\nivy.log.info("\=\=\=\=case id "+ivy.case.getId());
TaskA.TYPE=0' #txt
Ps0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>ProjectManager</name>
    </language>
</elementInfo>
' #txt
Ps0 f8 78 372 36 24 3 -32 #rect
Ps0 f8 @|UserTaskIcon #fIcon
Ps0 f2 processCall 'Functional Processes/SetAdditonalCaseDetailPage:call(String)' #txt
Ps0 f2 requestActionDecl '<String linkToAddtionalCaseDetailPage> param;' #txt
Ps0 f2 requestMappingAction 'param.linkToAddtionalCaseDetailPage="Start Processes/CustomizeDetailsPage/start.ivp";
' #txt
Ps0 f2 responseMappingAction 'out=in;
' #txt
Ps0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Functional Processes/SetAdditonalCaseDetailPage</name>
    </language>
</elementInfo>
' #txt
Ps0 f2 78 148 36 24 20 -4 #rect
Ps0 f2 @|CallSubIcon #fIcon
Ps0 f5 96 87 96 148 #arcP
Ps0 f9 96 296 96 372 #arcP
Ps0 f6 96 172 96 248 #arcP
>Proto Ps0 .type app.PromoteProcessData #txt
>Proto Ps0 .processKind NORMAL #txt
>Proto Ps0 0 0 32 24 18 0 #rect
>Proto Ps0 @|BIcon #fIcon
Ps0 f8 out f4 tail #connect
Ps0 f4 head f1 mainIn #connect
Ps0 f3 mainOut f9 tail #connect
Ps0 f9 head f8 in #connect
Ps0 f0 mainOut f5 tail #connect
Ps0 f5 head f2 mainIn #connect
Ps0 f2 mainOut f6 tail #connect
Ps0 f6 head f3 mainIn #connect
