[Ivy]
178205A2E252EB8C 7.5.0 #module
>Proto >Proto Collection #zClass
T20 Test2 Big #zClass
T20 B #cInfo
T20 #process
T20 @TextInP .type .type #zField
T20 @TextInP .processKind .processKind #zField
T20 @TextInP .xml .xml #zField
T20 @TextInP .responsibility .responsibility #zField
T20 @StartRequest f0 '' #zField
T20 @GridStep f1 '' #zField
T20 @GridStep f5 '' #zField
T20 @EndTask f7 '' #zField
T20 @PushWFArc f8 '' #zField
T20 @PushWFArc f6 '' #zField
T20 @GridStep f2 '' #zField
T20 @GridStep f3 '' #zField
T20 @PushWFArc f4 '' #zField
>Proto T20 T20 Test2 #zField
T20 f0 outLink start.ivp #txt
T20 f0 inParamDecl '<> param;' #txt
T20 f0 requestEnabled true #txt
T20 f0 triggerEnabled false #txt
T20 f0 callSignature start() #txt
T20 f0 caseData businessCase.attach=true #txt
T20 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
T20 f0 @C|.responsibility Everybody #txt
T20 f0 81 49 30 30 -21 4 #rect
T20 f0 @|StartRequestIcon #fIcon
T20 f1 actionTable 'out=in;
' #txt
T20 f1 actionCode 'import ch.ivyteam.ivy.workflow.ITask;
import ch.ivyteam.ivy.workflow.ICase;
import app.CaseUtils;
CaseUtils caseUtils;
ivy.log.info(caseUtils.findcase(1));
ICase case = caseUtils.findcase(1);
String var= case.customFields().stringField("customField1").toString();
ivy.log.info(var);' #txt
T20 f1 40 258 112 44 0 -8 #rect
T20 f1 @|StepIcon #fIcon
T20 f5 actionTable 'out=in;
' #txt
T20 f5 actionCode 'import entity.PromoteForm;
import repository.PromoteFormRepository;
import repository.EmployeeRepository;
import entity.Employee;

 
EmployeeRepository repo = new EmployeeRepository();
Employee emp1 = repo.searchByName("Thong pro");
ivy.log.info(emp1.name);


Employee emp2 = repo.searchByID("1");
ivy.log.info(emp2.name);

PromoteFormRepository formRepo = new repository.PromoteFormRepository();
PromoteForm form1 = formRepo.findPromoteFormByCaseId("123");
ivy.log.info("====== "+form1.employees.size());
' #txt
T20 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>create</name>
    </language>
</elementInfo>
' #txt
T20 f5 238 148 36 24 20 -2 #rect
T20 f5 @|StepIcon #fIcon
T20 f7 467 147 26 26 14 0 #rect
T20 f7 @|EndIcon #fIcon
T20 f8 274 160 467 160 #arcP
T20 f6 232 92 256 148 #arcP
T20 f2 actionTable 'out=in;
' #txt
T20 f2 actionCode 'import repository.PromoteFormRepository;
import entity.PromoteForm;
ivy.case.createNote(ivy.session, "message");

import entity.Employee;
Employee emp = new Employee();
emp.id = 1;
emp.age = 22;
emp.name = "Thong Pro";

ivy.repo.save(emp);

PromoteForm form = new PromoteForm();
form.employees.add(emp);
form.employees.add(emp);

form.caseId="123";
form.creator = "Linh";
form.comment = "OK";

ivy.log.info("step1 "+form.creator);
ivy.log.info(form.employees);
ivy.repo.save(form);' #txt
T20 f2 214 68 36 24 20 -2 #rect
T20 f2 @|StepIcon #fIcon
T20 f3 actionTable 'out=in;
' #txt
T20 f3 actionCode 'import entity.PromoteForm;
import repository.PromoteFormRepository;
import repository.EmployeeRepository;
import entity.Employee;

PromoteFormRepository formRepo = new repository.PromoteFormRepository();
PromoteForm form1 = formRepo.findPromoteFormByCaseId("271");
ivy.log.info("====== "+form1.employees.size());
' #txt
T20 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
T20 f3 94 132 36 24 20 -2 #rect
T20 f3 @|StepIcon #fIcon
T20 f4 98 78 112 132 #arcP
>Proto T20 .type app.Customer #txt
>Proto T20 .processKind NORMAL #txt
>Proto T20 0 0 32 24 18 0 #rect
>Proto T20 @|BIcon #fIcon
T20 f2 mainOut f6 tail #connect
T20 f6 head f5 mainIn #connect
T20 f5 mainOut f8 tail #connect
T20 f8 head f7 mainIn #connect
T20 f0 mainOut f4 tail #connect
T20 f4 head f3 mainIn #connect
