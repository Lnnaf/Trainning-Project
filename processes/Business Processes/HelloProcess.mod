[Ivy]
1788694E39ADA91A 7.5.0 #module
>Proto >Proto Collection #zClass
Hs0 HelloProcess Big #zClass
Hs0 B #cInfo
Hs0 #process
Hs0 @AnnotationInP-0n ai ai #zField
Hs0 @TextInP .type .type #zField
Hs0 @TextInP .processKind .processKind #zField
Hs0 @TextInP .xml .xml #zField
Hs0 @TextInP .responsibility .responsibility #zField
Hs0 @StartRequest f0 '' #zField
Hs0 @EndTask f1 '' #zField
Hs0 @UserDialog f3 '' #zField
Hs0 @PushWFArc f4 '' #zField
Hs0 @WSElement f2 '' #zField
Hs0 @PushWFArc f5 '' #zField
Hs0 @PushWFArc f7 '' #zField
Hs0 @PushWFArc f8 '' #zField
Hs0 @UserDialog f6 '' #zField
>Proto Hs0 Hs0 HelloProcess #zField
Hs0 f0 outLink start.ivp #txt
Hs0 f0 inParamDecl '<> param;' #txt
Hs0 f0 requestEnabled true #txt
Hs0 f0 triggerEnabled false #txt
Hs0 f0 callSignature start() #txt
Hs0 f0 caseData businessCase.attach=true #txt
Hs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Hs0 f0 @C|.responsibility Everybody #txt
Hs0 f0 80 48 32 32 -21 17 #rect
Hs0 f0 @|StartRequestIcon #fIcon
Hs0 f1 793 49 30 30 0 15 #rect
Hs0 f1 @|EndIcon #fIcon
Hs0 f3 dialogId app.Hello #txt
Hs0 f3 startMethod start() #txt
Hs0 f3 requestActionDecl '<> param;' #txt
Hs0 f3 responseMappingAction 'out.name=result.name;
' #txt
Hs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Hello</name>
    </language>
</elementInfo>
' #txt
Hs0 f3 176 42 112 44 -14 -8 #rect
Hs0 f3 @|UserDialogIcon #fIcon
Hs0 f4 112 64 176 64 #arcP
Hs0 f2 actionTable 'out=in;
out.name="Hello "+in.name;
' #txt
Hs0 f2 clientId 1787D32F8D2ABFCD #txt
Hs0 f2 port HelloServicePort #txt
Hs0 f2 operation call #txt
Hs0 f2 inputParams 'parameters.name=in.name;
' #txt
Hs0 f2 424 42 112 44 0 -8 #rect
Hs0 f2 @|WebServiceIcon #fIcon
Hs0 f5 288 64 424 64 #arcP
Hs0 f7 536 64 616 64 #arcP
Hs0 f8 728 64 793 64 #arcP
Hs0 f6 dialogId app.Hello #txt
Hs0 f6 startMethod startResult(String) #txt
Hs0 f6 requestActionDecl '<String name> param;' #txt
Hs0 f6 requestMappingAction 'param.name=in.name;
' #txt
Hs0 f6 responseMappingAction 'out=in;
' #txt
Hs0 f6 responseActionCode ivy.log.info(in.isResult); #txt
Hs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Hello</name>
    </language>
</elementInfo>
' #txt
Hs0 f6 616 42 112 44 -14 -8 #rect
Hs0 f6 @|UserDialogIcon #fIcon
>Proto Hs0 .type app.HelloProcessData #txt
>Proto Hs0 .processKind NORMAL #txt
>Proto Hs0 0 0 32 24 18 0 #rect
>Proto Hs0 @|BIcon #fIcon
Hs0 f0 mainOut f4 tail #connect
Hs0 f4 head f3 mainIn #connect
Hs0 f3 mainOut f5 tail #connect
Hs0 f5 head f2 mainIn #connect
Hs0 f2 mainOut f7 tail #connect
Hs0 f7 head f6 mainIn #connect
Hs0 f6 mainOut f8 tail #connect
Hs0 f8 head f1 mainIn #connect
