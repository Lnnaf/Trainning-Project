[Ivy]
1783E0657D8D3881 7.5.0 #module
>Proto >Proto Collection #zClass
ad0 CaseStudy Big #zClass
ad0 B #cInfo
ad0 #process
ad0 @AnnotationInP-0n ai ai #zField
ad0 @TextInP .type .type #zField
ad0 @TextInP .processKind .processKind #zField
ad0 @TextInP .xml .xml #zField
ad0 @TextInP .responsibility .responsibility #zField
ad0 @StartRequest f0 '' #zField
ad0 @UserDialog f3 '' #zField
ad0 @PushWFArc f5 '' #zField
ad0 @EndTask f1 '' #zField
ad0 @TaskSwitchSimple f4 '' #zField
ad0 @TkArc f6 '' #zField
ad0 @PushWFArc f2 '' #zField
>Proto ad0 ad0 CaseStudy #zField
ad0 f0 outLink start.ivp #txt
ad0 f0 inParamDecl '<> param;' #txt
ad0 f0 requestEnabled true #txt
ad0 f0 triggerEnabled false #txt
ad0 f0 callSignature start() #txt
ad0 f0 caseData businessCase.attach=true #txt
ad0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
ad0 f0 @C|.responsibility Everybody #txt
ad0 f0 81 57 30 30 -21 17 #rect
ad0 f0 @|StartRequestIcon #fIcon
ad0 f3 dialogId app.PromoteForm #txt
ad0 f3 startMethod start() #txt
ad0 f3 requestActionDecl '<> param;' #txt
ad0 f3 responseMappingAction 'out=in;
' #txt
ad0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>PromoteForm</name>
    </language>
</elementInfo>
' #txt
ad0 f3 184 50 112 44 -38 -8 #rect
ad0 f3 @|UserDialogIcon #fIcon
ad0 f5 111 72 184 72 #arcP
ad0 f1 480 64 32 32 0 46 #rect
ad0 f1 @|EndIcon #fIcon
ad0 f4 actionTable 'out=in1;
' #txt
ad0 f4 taskData 'TaskA.NAM=Task 1' #txt
ad0 f4 385 57 30 30 0 16 #rect
ad0 f4 @|TaskSwitchSimpleIcon #fIcon
ad0 f6 296 72 385 72 #arcP
ad0 f2 414 73 480 78 #arcP
>Proto ad0 .type app.Customer #txt
>Proto ad0 .processKind NORMAL #txt
>Proto ad0 0 0 32 24 18 0 #rect
>Proto ad0 @|BIcon #fIcon
ad0 f0 mainOut f5 tail #connect
ad0 f5 head f3 mainIn #connect
ad0 f3 mainOut f6 tail #connect
ad0 f6 head f4 in #connect
ad0 f4 out f2 tail #connect
ad0 f2 head f1 mainIn #connect
