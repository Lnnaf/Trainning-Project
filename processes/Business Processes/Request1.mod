[Ivy]
17810291145BE0E5 7.5.0 #module
>Proto >Proto Collection #zClass
R10 Request1 Big #zClass
R10 B #cInfo
R10 #process
R10 @AnnotationInP-0n ai ai #zField
R10 @TextInP .type .type #zField
R10 @TextInP .processKind .processKind #zField
R10 @TextInP .xml .xml #zField
R10 @TextInP .responsibility .responsibility #zField
R10 @StartRequest f0 '' #zField
R10 @DBStep f3 '' #zField
R10 @PushWFArc f10 '' #zField
R10 @UserDialog f1 '' #zField
R10 @PushWFArc f6 '' #zField
>Proto R10 R10 Request1 #zField
R10 f0 outLink start.ivp #txt
R10 f0 inParamDecl '<> param;' #txt
R10 f0 requestEnabled true #txt
R10 f0 triggerEnabled false #txt
R10 f0 callSignature start() #txt
R10 f0 caseData businessCase.attach=true #txt
R10 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
R10 f0 @C|.responsibility Everybody #txt
R10 f0 208 52 32 24 -18 12 #rect
R10 f0 @|StartRequestIcon #fIcon
R10 f3 actionTable 'out=in;
' #txt
R10 f3 actionCode 'import ch.ivyteam.ivy.webservice.datamodel.internal.Info;
ivy.log.info(recordset);
if(recordset.size()==0)
{
	ivy.log.info("no record found");
}else{
	for(int i=0;i<recordset.size();i++){
		Record record = recordset.getAt(i);
		int id = record.getField("id").toNumber();
		String name = record.getField("name").toString();
		int age = record.getField("age").toNumber();
		String address = record.getField("address").toString();
		String imgUrl = record.getField("img_url").toString();
		out.customers.add((new app.Customer())
		.setId(id)
		.setName(name)
		.setAge(age)
		.setAddress(address)
		.setImgUrl(imgUrl));
	}
};

' #txt
R10 f3 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE SELECT SYSTEM  ""sqlStatements.dtd"">
<SELECT><Table name=''customer''/><OrderBy direction=''DESC''><Column name=''id''/></OrderBy></SELECT>' #txt
R10 f3 dbUrl customerConector #txt
R10 f3 lotSize 2147483647 #txt
R10 f3 startIdx 0 #txt
R10 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get All Customers</name>
    </language>
</elementInfo>
' #txt
R10 f3 312 42 112 44 -50 -8 #rect
R10 f3 @|DBStepIcon #fIcon
R10 f10 240 64 312 64 #arcP
R10 f1 dialogId app.outCus #txt
R10 f1 startMethod start(app.Customer,List<app.Customer>) #txt
R10 f1 requestActionDecl '<app.Customer customer,List<app.Customer> customers> param;' #txt
R10 f1 requestMappingAction 'param.customers=in.customers;
' #txt
R10 f1 responseMappingAction 'out=in;
' #txt
R10 f1 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Display Customer View</name>
    </language>
</elementInfo>
' #txt
R10 f1 464 42 144 44 -64 -8 #rect
R10 f1 @|UserDialogIcon #fIcon
R10 f6 424 64 464 64 #arcP
>Proto R10 .type app.Customer #txt
>Proto R10 .processKind NORMAL #txt
>Proto R10 0 0 32 24 18 0 #rect
>Proto R10 @|BIcon #fIcon
R10 f0 mainOut f10 tail #connect
R10 f10 head f3 mainIn #connect
R10 f3 mainOut f6 tail #connect
R10 f6 head f1 mainIn #connect
