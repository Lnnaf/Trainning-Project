[Ivy]
178B582AA431E217 7.5.0 #module
>Proto >Proto Collection #zClass
Ce0 CustomizeDetailsPage Big #zClass
Ce0 B #cInfo
Ce0 #process
Ce0 @AnnotationInP-0n ai ai #zField
Ce0 @TextInP .type .type #zField
Ce0 @TextInP .processKind .processKind #zField
Ce0 @TextInP .xml .xml #zField
Ce0 @TextInP .responsibility .responsibility #zField
Ce0 @StartRequest f0 '' #zField
Ce0 @EndTask f1 '' #zField
Ce0 @UserDialog f3 '' #zField
Ce0 @PushWFArc f2 '' #zField
Ce0 @GridStep f5 '' #zField
Ce0 @PushWFArc f6 '' #zField
Ce0 @PushWFArc f4 '' #zField
>Proto Ce0 Ce0 CustomizeDetailsPage #zField
Ce0 f0 outLink start.ivp #txt
Ce0 f0 inParamDecl '<String caseId> param;' #txt
Ce0 f0 inParamTable 'out.caseId=param.caseId.toNumber();
' #txt
Ce0 f0 requestEnabled true #txt
Ce0 f0 triggerEnabled false #txt
Ce0 f0 callSignature start(String) #txt
Ce0 f0 caseData businessCase.attach=true #txt
Ce0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Ce0 f0 @C|.responsibility Everybody #txt
Ce0 f0 91 51 26 26 14 0 #rect
Ce0 f0 @|StartRequestIcon #fIcon
Ce0 f1 531 51 26 26 14 0 #rect
Ce0 f1 @|EndIcon #fIcon
Ce0 f3 dialogId app.PromoteForm #txt
Ce0 f3 startMethod startDetails(entity.PromoteForm,List<entity.Employee>) #txt
Ce0 f3 requestActionDecl '<entity.PromoteForm promoteForm,List<entity.Employee> employees> param;' #txt
Ce0 f3 requestMappingAction 'param.promoteForm=in.promoteForm;
param.employees=in.promoteForm.employees;
' #txt
Ce0 f3 responseMappingAction 'out=in;
' #txt
Ce0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>PromoteForm</name>
    </language>
</elementInfo>
' #txt
Ce0 f3 310 52 36 24 20 -2 #rect
Ce0 f3 @|UserDialogIcon #fIcon
Ce0 f2 346 64 531 64 #arcP
Ce0 f5 actionTable 'out=in;
' #txt
Ce0 f5 actionCode 'import entity.PromoteForm;
import repository.PromoteFormRepository;
import repository.EmployeeRepository;
import entity.Employee;

PromoteFormRepository formRepo = new repository.PromoteFormRepository();
PromoteForm form1 = formRepo.findPromoteFormByCaseId(in.caseId.toString());
in.promoteForm = form1;
' #txt
Ce0 f5 190 52 36 24 20 -2 #rect
Ce0 f5 @|StepIcon #fIcon
Ce0 f6 117 64 190 64 #arcP
Ce0 f4 226 64 310 64 #arcP
>Proto Ce0 .type app.PromoteProcessData #txt
>Proto Ce0 .processKind NORMAL #txt
>Proto Ce0 0 0 32 24 18 0 #rect
>Proto Ce0 @|BIcon #fIcon
Ce0 f3 mainOut f2 tail #connect
Ce0 f2 head f1 mainIn #connect
Ce0 f0 mainOut f6 tail #connect
Ce0 f6 head f5 mainIn #connect
Ce0 f5 mainOut f4 tail #connect
Ce0 f4 head f3 mainIn #connect
