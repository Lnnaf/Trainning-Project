package entity;

public @interface StartsWith {

	String prefix();

}
