package entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import ch.ivyteam.ivy.business.data.store.context.BusinessCaseData;

@Entity
@BusinessCaseData
public class RequestPromote {
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private int id;
	 
	 @ManyToOne
	 @JoinColumn(name = "promoteForm_id")
	 private PromoteForm promoteForm;
	 
	 @ManyToOne
	 @JoinColumn(name =  "employee_id")
	 private Employee employee;
	 
	 private String caseId;

	public RequestPromote() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RequestPromote(int id, PromoteForm promoteForm, Employee employee, String caseId) {
		super();
		this.id = id;
		this.promoteForm = promoteForm;
		this.employee = employee;
		this.caseId = caseId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PromoteForm getPromoteForm() {
		return promoteForm;
	}

	public void setPromoteForm(PromoteForm promoteForm) {
		this.promoteForm = promoteForm;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	
	
	 
	 

}
