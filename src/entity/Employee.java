package entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import ch.ivyteam.ivy.business.data.store.context.BusinessCaseData;



@Entity
@BusinessCaseData
public class Employee {


	/**
	 * 
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotNull(message = "Name: Field is required !")
	private String name;
	@NotNull(message = "Age: Field is required !")
	private Integer age;
	@NotNull(message = "Team: Field is required !")
	private String team;
//	@OneToMany(mappedBy = "employee")
//	Set<RequestPromote> requestPromotes;
	



	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Employee(Integer id, String name, Integer age, String team) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.team = team;
	
	}
	public Employee(String name, Integer age, String team) {
		super();
		this.name = name;
		this.age = age;
		this.team = team;
	
	}
	
	



//	public Employee(Integer id, String name, Integer age, String team) {
//		super();
//		this.id = id;
//		this.name = name;
//		this.age = age;
//		this.team = team;
////		this.requestPromotes = requestPromotes;
//	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public Integer getAge() {
		return age;
	}



	public void setAge(Integer age) {
		this.age = age;
	}



	public String getTeam() {
		return team;
	}



	public void setTeam(String team) {
		this.team = team;
	}

//
//
//	public Set<RequestPromote> getRequestPromotes() {
//		return requestPromotes;
//	}
//
//
//
//	public void setRequestPromotes(Set<RequestPromote> requestPromotes) {
//		this.requestPromotes = requestPromotes;
//	}





	
	
}
