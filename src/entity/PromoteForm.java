package entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import ch.ivyteam.ivy.business.data.store.context.BusinessCaseData;



@Entity
@BusinessCaseData
@Table(name = "promote_form")
public class PromoteForm {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@NotNull(message = "<%=ivy.cms.co(\"/ValidateBean/BeanValidationDemo/notnull\")%>")

	private String creator;
	private Date dateSubmit;
	private String comment;
	private String caseId;
	@OneToMany(mappedBy = "promoteForm")
	List<Employee> employees;

	public PromoteForm() {
		super();
	}

	public PromoteForm(int id, String creator, Date dateSubmit, String comment,String caseId, List<Employee> employees) {
		super();
		this.id = id;
		this.creator = creator;
		this.dateSubmit = dateSubmit;
		this.comment = comment;
		this.employees = employees;
		this.caseId = caseId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getDateSubmit() {
		return dateSubmit;
	}

	public void setDateSubmit(Date dateSubmit) {
		this.dateSubmit = dateSubmit;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	
	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	

	

}
