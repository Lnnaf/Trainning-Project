package repository;

import ch.ivyteam.ivy.environment.Ivy;
import entity.PromoteForm;

public class PromoteFormRepository {
	public PromoteForm findPromoteFormById(String id) {
		PromoteForm form = Ivy.repo().find(id, PromoteForm.class);
		return form;
	}
	
	public PromoteForm findPromoteFormByCaseId(String caseId) {
		PromoteForm result = Ivy.repo().search(PromoteForm.class)
			       .textField("caseId").containsAnyWords(caseId)
			       .execute()
			       .getFirst();
		return result;
	}
	
	public void save(PromoteForm form) {
	Ivy.repo().save(form);
	}

}
