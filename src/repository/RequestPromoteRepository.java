package repository;

import javax.faces.bean.ManagedBean;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryRegistry;

import ch.ivyteam.ivy.environment.Ivy;
import entity.Employee;
import entity.RequestPromote;
@ManagedBean

public class RequestPromoteRepository {
	public Employee findById(String id) {
		Employee emp = Ivy.repo().find(id, Employee.class);
		return emp;
	}
	
	public Employee searchByName(String name) {
		Employee result = Ivy.repo().search(Employee.class)
			       .textField("name").containsAnyWords(name)
			       .execute()
			       .getFirst();
		return result;
	}
	
	
	
	public Employee searchByID(String id) {
		Employee result = Ivy.repo().search(Employee.class)
			       .textField("id").containsAnyWords(id)
			       .execute()
			       .getFirst();
		return result;
	}

}
