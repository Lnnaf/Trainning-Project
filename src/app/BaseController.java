package app;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class BaseController {
	public void redirect(String url) {
	    try {
	      FacesContext.getCurrentInstance().getExternalContext().redirect(url);
	    } catch (IOException ex) {
	      ex.printStackTrace();
	    }
	  }

}
