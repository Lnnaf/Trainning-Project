package app;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;



import org.primefaces.event.SelectEvent;

import entity.Employee;

@ManagedBean
@ViewScoped
public class DtSelection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Employee selectedProduct;

	public Employee getSelectedProduct() {
		return selectedProduct;
	}



	public void setSelectedProduct(Employee selectedProduct) {
		this.selectedProduct = selectedProduct;
	}
	

	 public Employee onRowSelect(SelectEvent event) {
			return (Employee) event.getObject();
	    }

}
