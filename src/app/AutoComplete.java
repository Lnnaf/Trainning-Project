package app;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;

import com.google.inject.servlet.SessionScoped;

@ManagedBean
@SessionScoped
public class AutoComplete {

	 public List<String> completeText(String query) {
	
			List<String> listTeam = new ArrayList<>();
			listTeam.add("Bolt");
			listTeam.add("Everest");
			listTeam.add("Crocodie");
			listTeam.add("Bamboo");
			listTeam.add("Gun8");
			listTeam.add("Bingo");
			listTeam.add("SunWheel");
			listTeam.add("Anonymous");
			listTeam.add("Fusion");
			listTeam.add("Apolo");
			listTeam.add("Skynet");
			listTeam.add("Bee12");
			listTeam.add("Hulk");
			listTeam.add("Uranus");
			listTeam.add("Europa");
			listTeam.add("Nemo");
			listTeam.add("Kangaroo");
			listTeam.add("Mercury");
			listTeam.add("Scuty");
			listTeam.add("Koala");
			listTeam.add("Ninja");
	   
	          return listTeam.stream().filter(t -> t.toLowerCase().startsWith(query.toLowerCase())).collect(Collectors.toList());
	    
	 }

	 
}
