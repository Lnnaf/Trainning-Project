package app;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.server.ServerFactory;
import ch.ivyteam.ivy.workflow.ICase;


public class CaseUtils {
	
	 public ICase findcase(final long id) {
		    try {
		      return ServerFactory.getServer().getSecurityManager().executeAsSystem(() -> {
		        return Ivy.wf().findCase(id);
		      });
		    } catch (Exception e) {
		      Ivy.log().error(e);
		    }
		    return null;
		  }

}
