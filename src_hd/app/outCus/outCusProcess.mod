[Ivy]
17819D32BE4D6AA3 7.5.0 #module
>Proto >Proto Collection #zClass
os0 outCusProcess Big #zClass
os0 RD #cInfo
os0 #process
os0 @TextInP .type .type #zField
os0 @TextInP .processKind .processKind #zField
os0 @TextInP .xml .xml #zField
os0 @TextInP .responsibility .responsibility #zField
os0 @UdInit f0 '' #zField
os0 @UdEvent f3 '' #zField
os0 @UdExitEnd f4 '' #zField
os0 @PushWFArc f5 '' #zField
os0 @UdEvent f6 '' #zField
os0 @DBStep f7 '' #zField
os0 @UdProcessEnd f9 '' #zField
os0 @UdProcessEnd f1 '' #zField
os0 @UdMethod f17 '' #zField
os0 @UdEvent f11 '' #zField
os0 @Alternative f15 '' #zField
os0 @DBStep f22 '' #zField
os0 @PushWFArc f23 '' #zField
os0 @PushWFArc f10 '' #zField
os0 @DBStep f12 '' #zField
os0 @DBStep f27 '' #zField
os0 @Alternative f31 '' #zField
os0 @PushWFArc f28 '' #zField
os0 @UdEvent f34 '' #zField
os0 @UdProcessEnd f33 '' #zField
os0 @PushWFArc f37 '' #zField
os0 @UdMethod f39 '' #zField
os0 @UdProcessEnd f40 '' #zField
os0 @DBStep f41 '' #zField
os0 @PushWFArc f18 '' #zField
os0 @DBStep f43 '' #zField
os0 @PushWFArc f30 '' #zField
os0 @PushWFArc f45 '' #zField
os0 @UdEvent f48 '' #zField
os0 @Alternative f8 '' #zField
os0 @PushWFArc f53 '' #zField
os0 @PushWFArc f55 '' #zField
os0 @GridStep f54 '' #zField
os0 @PushWFArc f13 '' #zField
os0 @GridStep f57 '' #zField
os0 @PushWFArc f44 '' #zField
os0 @GridStep f59 '' #zField
os0 @PushWFArc f60 '' #zField
os0 @PushWFArc f42 '' #zField
os0 @DBStep f70 '' #zField
os0 @PushWFArc f69 '' #zField
os0 @PushWFArc f66 '' #zField
os0 @GridStep f67 '' #zField
os0 @PushWFArc f2 '' #zField
os0 @PushWFArc f16 '' #zField
os0 @PushWFArc f47 '' #zField
os0 @PushWFArc f51 '' #zField
os0 @PushWFArc f49 '' #zField
os0 @PushWFArc f21 '' #zField
os0 @PushWFArc f14 '' #zField
os0 @PushWFArc f19 '' #zField
>Proto os0 os0 outCusProcess #zField
os0 f0 guid 17819D32BEC50E53 #txt
os0 f0 method start(app.Customer,List<app.Customer>) #txt
os0 f0 inParameterDecl '<app.Customer customer,List<app.Customer> customers> param;' #txt
os0 f0 inParameterMapAction 'out.customers=param.customers;
' #txt
os0 f0 outParameterDecl '<app.Customer customer> result;' #txt
os0 f0 outParameterMapAction 'result.customer=in.customer;
' #txt
os0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start(Customer,List&lt;Customer&gt;)</name>
    </language>
</elementInfo>
' #txt
os0 f0 43 51 26 26 -43 15 #rect
os0 f0 @|UdInitIcon #fIcon
os0 f3 guid 17819D32BFBEB872 #txt
os0 f3 actionTable 'out=in;
' #txt
os0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
os0 f3 35 99 26 26 -15 15 #rect
os0 f3 @|UdEventIcon #fIcon
os0 f4 163 99 26 26 0 12 #rect
os0 f4 @|UdExitEndIcon #fIcon
os0 f5 61 112 163 112 #arcP
os0 f6 guid 17819EFE30CFDAEE #txt
os0 f6 actionTable 'out=in;
out.actionName="Add new customer";
out.customer=null;
out.isConfirm=false;
' #txt
os0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>create</name>
    </language>
</elementInfo>
' #txt
os0 f6 43 171 26 26 -14 15 #rect
os0 f6 @|UdEventIcon #fIcon
os0 f7 actionTable 'out=in;
' #txt
os0 f7 actionCode 'import app.Customer;
out.customer = new Customer();' #txt
os0 f7 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE INSERT SYSTEM  ""sqlStatements.dtd"">
<INSERT><Table name=''customer''/><Value column=''name''><AnyExpression>in.customer.name</AnyExpression></Value><Value column=''age''><AnyExpression>in.customer.age</AnyExpression></Value><Value column=''address''><AnyExpression>in.customer.address</AnyExpression></Value></INSERT>' #txt
os0 f7 dbUrl customerConector #txt
os0 f7 lotSize 2147483647 #txt
os0 f7 startIdx 0 #txt
os0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>add new customer</name>
    </language>
</elementInfo>
' #txt
os0 f7 312 242 112 44 -51 -8 #rect
os0 f7 @|DBStepIcon #fIcon
os0 f9 723 171 26 26 0 12 #rect
os0 f9 @|UdProcessEndIcon #fIcon
os0 f1 723 51 26 26 0 12 #rect
os0 f1 @|UdProcessEndIcon #fIcon
os0 f17 guid 1781B29AE8E812AC #txt
os0 f17 method delete(Integer) #txt
os0 f17 inParameterDecl '<Integer id> param;' #txt
os0 f17 inParameterMapAction 'out.customer.id=param.id;
' #txt
os0 f17 inActionCode 'out.isConfirm = false;
' #txt
os0 f17 outParameterDecl '<> result;' #txt
os0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete(Integer)</name>
    </language>
</elementInfo>
' #txt
os0 f17 51 411 26 26 -25 15 #rect
os0 f17 @|UdMethodIcon #fIcon
os0 f11 guid 01781B6D25266734 #txt
os0 f11 actionTable 'out=in;
' #txt
os0 f11 actionCode 'out.isConfirm = true;' #txt
os0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isConfirm</name>
    </language>
</elementInfo>
' #txt
os0 f11 51 459 26 26 -18 15 #rect
os0 f11 @|UdEventIcon #fIcon
os0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>is Deleted ?</name>
    </language>
</elementInfo>
' #txt
os0 f15 344 408 32 32 -29 -40 #rect
os0 f15 @|AlternativeIcon #fIcon
os0 f22 actionTable 'out=in;
' #txt
os0 f22 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE DELETE SYSTEM  ""sqlStatements.dtd"">
<DELETE><Table name=''customer''/><Condition><Relation kind=''=''><Column name=''id''/><AnyExpression>in.customer.id</AnyExpression></Relation></Condition></DELETE>' #txt
os0 f22 dbUrl customerConector #txt
os0 f22 dbWizard id=in.customer.id #txt
os0 f22 lotSize 2147483647 #txt
os0 f22 startIdx 0 #txt
os0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>DeleteData</name>
    </language>
</elementInfo>
' #txt
os0 f22 304 474 112 44 -31 -8 #rect
os0 f22 @|DBStepIcon #fIcon
os0 f23 expr in #txt
os0 f23 outCond 'in.isConfirm !=false' #txt
os0 f23 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Yes</name>
    </language>
</elementInfo>
' #txt
os0 f23 360 440 360 474 #arcP
os0 f23 0 0.20588235294117646 20 -1 #arcLabel
os0 f10 736 242 736 197 #arcP
os0 f12 actionTable 'out=in;
' #txt
os0 f12 actionCode 'import ch.ivyteam.ivy.webservice.datamodel.internal.Info;
// Clear data before update
out.customers.clear();
if(recordset.size()==0)
{
	ivy.log.info("no record found");
}else{
	for(int i=0;i<recordset.size();i++){
		Record record = recordset.getAt(i);
		int id = record.getField("id").toNumber();
		String name = record.getField("name").toString();
		int age = record.getField("age").toNumber();
		String address = record.getField("address").toString();
		out.customers.add((new app.Customer())
		.setId(id)
		.setName(name)
		.setAge(age)
		.setAddress(address));
	}
};

' #txt
os0 f12 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE SELECT SYSTEM  ""sqlStatements.dtd"">
<SELECT><Table name=''customer''/></SELECT>' #txt
os0 f12 dbUrl customerConector #txt
os0 f12 lotSize 2147483647 #txt
os0 f12 startIdx 0 #txt
os0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>ReadAllData</name>
    </language>
</elementInfo>
' #txt
os0 f12 680 242 112 44 -34 -8 #rect
os0 f12 @|DBStepIcon #fIcon
os0 f27 actionTable 'out=in;
' #txt
os0 f27 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE UPDATE SYSTEM  ""sqlStatements.dtd"">
<UPDATE><Table name=''customer''/><Value column=''name''><AnyExpression>in.customer.name</AnyExpression></Value><Value column=''age''><AnyExpression>in.customer.age</AnyExpression></Value><Value column=''address''><AnyExpression>in.customer.address</AnyExpression></Value><Condition><Relation kind=''=''><Column name=''id''/><AnyExpression>in.customer.id</AnyExpression></Relation></Condition></UPDATE>' #txt
os0 f27 dbUrl customerConector #txt
os0 f27 dbWizard id=in.customer.id #txt
os0 f27 lotSize 2147483647 #txt
os0 f27 startIdx 0 #txt
os0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>updateData</name>
    </language>
</elementInfo>
' #txt
os0 f27 296 698 112 44 -32 -8 #rect
os0 f27 @|DBStepIcon #fIcon
os0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>is Update?</name>
    </language>
</elementInfo>
' #txt
os0 f31 336 600 32 32 -26 -40 #rect
os0 f31 @|AlternativeIcon #fIcon
os0 f28 expr in #txt
os0 f28 outCond in.isEdit!=false #txt
os0 f28 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Yes&#13;
</name>
    </language>
</elementInfo>
' #txt
os0 f28 352 632 352 698 #arcP
os0 f28 0 0.5126698857481502 0 0 #arcLabel
os0 f34 guid 1781BB47AD3DAE08 #txt
os0 f34 actionTable 'out=in;
out.isEdit=true;
' #txt
os0 f34 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isEdit</name>
    </language>
</elementInfo>
' #txt
os0 f34 51 667 26 26 -18 15 #rect
os0 f34 @|UdEventIcon #fIcon
os0 f33 739 411 26 26 0 12 #rect
os0 f33 @|UdProcessEndIcon #fIcon
os0 f37 expr in #txt
os0 f37 outCond 'in.isConfirm !=true' #txt
os0 f37 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>No&#13;
</name>
    </language>
</elementInfo>
' #txt
os0 f37 376 424 739 424 #arcP
os0 f37 0 0.5384615384615383 0 0 #arcLabel
os0 f39 guid 1781F0FB826DDFD9 #txt
os0 f39 method preUpdate(app.Customer) #txt
os0 f39 inParameterDecl '<app.Customer customer> param;' #txt
os0 f39 inParameterMapAction 'out.actionName="Edit customer";
out.customer=param.customer;
out.isEdit=false;
' #txt
os0 f39 outParameterDecl '<> result;' #txt
os0 f39 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>preUpdate(Customer)</name>
    </language>
</elementInfo>
' #txt
os0 f39 51 603 26 26 -25 15 #rect
os0 f39 @|UdMethodIcon #fIcon
os0 f40 731 603 26 26 0 12 #rect
os0 f40 @|UdProcessEndIcon #fIcon
os0 f41 actionTable 'out=in;
' #txt
os0 f41 actionCode 'import ch.ivyteam.ivy.webservice.datamodel.internal.Info;
//Clear data before update
out.customers.clear();
if(recordset.size()==0)
{
	ivy.log.info("no record found");
}else{
	for(int i=0;i<recordset.size();i++){
		Record record = recordset.getAt(i);
		int id = record.getField("id").toNumber();
		String name = record.getField("name").toString();
		int age = record.getField("age").toNumber();
		String address = record.getField("address").toString();
		out.customers.add((new app.Customer())
		.setId(id)
		.setName(name)
		.setAge(age)
		.setAddress(address));
	}
};

' #txt
os0 f41 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE SELECT SYSTEM  ""sqlStatements.dtd"">
<SELECT><Table name=''customer''/></SELECT>' #txt
os0 f41 dbUrl customerConector #txt
os0 f41 lotSize 2147483647 #txt
os0 f41 startIdx 0 #txt
os0 f41 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>ReadAllData</name>
    </language>
</elementInfo>
' #txt
os0 f41 696 474 112 44 -34 -8 #rect
os0 f41 @|DBStepIcon #fIcon
os0 f18 752 474 752 437 #arcP
os0 f18 0 0.5670824422455637 0 0 #arcLabel
os0 f43 actionTable 'out=in;
' #txt
os0 f43 actionCode 'import ch.ivyteam.ivy.webservice.datamodel.internal.Info;
ivy.log.info(recordset);
out.customers.clear();
if(recordset.size()==0)
{
ivy.log.info("no record found");
}else{
for(int i=0;i<recordset.size();i++){
Record record = recordset.getAt(i);
int id = record.getField("id").toNumber();
String name = record.getField("name").toString();
int age = record.getField("age").toNumber();
String address = record.getField("address").toString();
out.customers.add((new app.Customer())
.setId(id)
.setName(name)
.setAge(age)
.setAddress(address));
}
};
ivy.log.info(out.customers);
' #txt
os0 f43 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE SELECT SYSTEM  ""sqlStatements.dtd"">
<SELECT><Table name=''customer''/></SELECT>' #txt
os0 f43 dbUrl customerConector #txt
os0 f43 lotSize 2147483647 #txt
os0 f43 startIdx 0 #txt
os0 f43 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>ReadAllData</name>
    </language>
</elementInfo>
' #txt
os0 f43 688 698 112 44 -34 -8 #rect
os0 f43 @|DBStepIcon #fIcon
os0 f30 744 698 744 629 #arcP
os0 f45 expr in #txt
os0 f45 outCond in.isEdit!=true #txt
os0 f45 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>No</name>
    </language>
</elementInfo>
' #txt
os0 f45 368 616 731 616 #arcP
os0 f45 0 0.4738292011019284 0 -14 #arcLabel
os0 f48 guid 178249224892F59C #txt
os0 f48 actionTable 'out=in;
out.isConfirm=true;
' #txt
os0 f48 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isAdd</name>
    </language>
</elementInfo>
' #txt
os0 f48 43 235 26 26 -18 15 #rect
os0 f48 @|UdEventIcon #fIcon
os0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Is Edited ?</name>
    </language>
</elementInfo>
' #txt
os0 f8 352 168 32 32 -25 -39 #rect
os0 f8 @|AlternativeIcon #fIcon
os0 f53 expr in #txt
os0 f53 outCond 'in.isConfirm !=false' #txt
os0 f53 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Yes&#13;
</name>
    </language>
</elementInfo>
' #txt
os0 f53 368 200 368 242 #arcP
os0 f53 0 0.5 12 0 #arcLabel
os0 f55 expr in #txt
os0 f55 outCond 'in.isConfirm !=true' #txt
os0 f55 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>No&#13;
</name>
    </language>
</elementInfo>
' #txt
os0 f55 384 184 723 184 #arcP
os0 f55 0 0.7106586527840775 0 0 #arcLabel
os0 f54 actionTable 'out=in;
' #txt
os0 f54 actionCode 'import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;
 FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success !", "New customer was added"));' #txt
os0 f54 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show message</name>
    </language>
</elementInfo>
' #txt
os0 f54 512 242 112 44 -43 -8 #rect
os0 f54 @|StepIcon #fIcon
os0 f13 624 264 680 264 #arcP
os0 f57 actionTable 'out=in;
' #txt
os0 f57 actionCode 'import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;
 FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "New information was updated"));' #txt
os0 f57 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show message</name>
    </language>
</elementInfo>
' #txt
os0 f57 512 698 112 44 -43 -8 #rect
os0 f57 @|StepIcon #fIcon
os0 f44 624 720 688 720 #arcP
os0 f59 actionTable 'out=in;
' #txt
os0 f59 actionCode 'import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;
 FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Success !", "UID "+in.customer.id+" was deleted !"));
                ' #txt
os0 f59 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show message</name>
    </language>
</elementInfo>
' #txt
os0 f59 512 474 112 44 -43 -8 #rect
os0 f59 @|StepIcon #fIcon
os0 f60 416 496 512 496 #arcP
os0 f42 624 496 696 496 #arcP
os0 f70 actionTable 'out=in;
' #txt
os0 f70 actionCode 'import ch.ivyteam.ivy.webservice.datamodel.internal.Info;
out.listAge.clear();
if(recordset.size()==0)
{
ivy.log.info("no record found");
}else{
for(int i=0;i<recordset.size();i++){
Record record = recordset.getAt(i);
int age = record.getField("age").toNumber();
out.listAge.add(age);
}
};

' #txt
os0 f70 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE SELECT SYSTEM  ""sqlStatements.dtd"">
<SELECT><Column name=''age''/><Table name=''customer''/></SELECT>' #txt
os0 f70 dbUrl customerConector #txt
os0 f70 lotSize 2147483647 #txt
os0 f70 startIdx 0 #txt
os0 f70 88 864 144 48 0 -8 #rect
os0 f70 @|DBStepIcon #fIcon
os0 f69 408 720 512 720 #arcP
os0 f66 232 888 336 888 #arcP
os0 f67 actionTable 'out=in;
' #txt
os0 f67 actionCode 'import org.primefaces.model.charts.pie.PieChartModel;

import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.ChartData;
PieChartModel pieModel;
        ChartData data = new ChartData();
        
        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> values;
	         int younger = 0;
	        int middle = 0;
	        int old = 0;
	        ivy.log.info("list age"+out.listAge);
	        for(int age:out.listAge){
	        	if(age<20){
	        			younger++;
	        		}else if(age>20 && age<40){
	        			middle++;
	        		}else if(age>40){
	        		old++;
	        		}
	        }
	        values.add(younger);
	        values.add(middle);
	        values.add(old);
	        dataSet.setData(values);
        
        List<String> bgColors;
        bgColors.add("rgb(255, 99, 132)");
        bgColors.add("rgb(54, 162, 235)");
        bgColors.add("rgb(255, 205, 86)");
        dataSet.setBackgroundColor(bgColors);
        
        data.addChartDataSet(dataSet);
        List<String> labels;
        labels.add("Age <20");
        labels.add("Age 21 - 40");
        labels.add("age > 40");
        data.setLabels(labels);
        
        pieModel.setData(data);
	      out.dataChart = pieModel;
	      
	     ' #txt
os0 f67 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Data Chart&#13;
</name>
    </language>
</elementInfo>
' #txt
os0 f67 336 866 128 44 -40 -16 #rect
os0 f67 @|StepIcon #fIcon
os0 f2 69 64 723 64 #arcP
os0 f16 424 264 512 264 #arcP
os0 f47 69 184 352 184 #arcP
os0 f47 0 0.5577707927430928 0 0 #arcLabel
os0 f51 69 248 359 191 #arcP
os0 f51 1 288 248 #addKink
os0 f51 0 0.9354590635508497 0 0 #arcLabel
os0 f49 77 424 344 424 #arcP
os0 f21 77 472 350 430 #arcP
os0 f21 1 280 472 #addKink
os0 f21 1 0.583050856509355 0 0 #arcLabel
os0 f14 77 616 336 616 #arcP
os0 f19 77 680 345 625 #arcP
os0 f19 1 304 680 #addKink
os0 f19 0 0.6542142911124923 0 0 #arcLabel
>Proto os0 .type app.outCus.outCusData #txt
>Proto os0 .processKind HTML_DIALOG #txt
>Proto os0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel>Create Customer
</swimlaneLabel>
        <swimlaneLabel>Delete Customer</swimlaneLabel>
        <swimlaneLabel>Update Information</swimlaneLabel>
        <swimlaneLabel>Detail Customer</swimlaneLabel>
    </language>
    <swimlaneOrientation>false</swimlaneOrientation>
    <swimlaneSize>344</swimlaneSize>
    <swimlaneSize>224</swimlaneSize>
    <swimlaneSize>232</swimlaneSize>
    <swimlaneSize>192</swimlaneSize>
    <swimlaneColor gradient="false">-1</swimlaneColor>
    <swimlaneColor gradient="false">-3342337</swimlaneColor>
    <swimlaneColor gradient="false">-1</swimlaneColor>
    <swimlaneColor gradient="false">-3342337</swimlaneColor>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
</elementInfo>
' #txt
>Proto os0 -8 -8 16 16 16 26 #rect
>Proto os0 '' #fIcon
os0 f3 mainOut f5 tail #connect
os0 f5 head f4 mainIn #connect
os0 f12 mainOut f10 tail #connect
os0 f10 head f9 mainIn #connect
os0 f15 out f23 tail #connect
os0 f23 head f22 mainIn #connect
os0 f31 out f28 tail #connect
os0 f28 head f27 mainIn #connect
os0 f15 out f37 tail #connect
os0 f37 head f33 mainIn #connect
os0 f41 mainOut f18 tail #connect
os0 f18 head f33 mainIn #connect
os0 f43 mainOut f30 tail #connect
os0 f30 head f40 mainIn #connect
os0 f31 out f45 tail #connect
os0 f45 head f40 mainIn #connect
os0 f8 out f53 tail #connect
os0 f53 head f7 mainIn #connect
os0 f8 out f55 tail #connect
os0 f55 head f9 mainIn #connect
os0 f54 mainOut f13 tail #connect
os0 f13 head f12 mainIn #connect
os0 f57 mainOut f44 tail #connect
os0 f44 head f43 mainIn #connect
os0 f22 mainOut f60 tail #connect
os0 f60 head f59 mainIn #connect
os0 f59 mainOut f42 tail #connect
os0 f42 head f41 mainIn #connect
os0 f70 mainOut f66 tail #connect
os0 f66 head f67 mainIn #connect
os0 f27 mainOut f69 tail #connect
os0 f69 head f57 mainIn #connect
os0 f0 mainOut f2 tail #connect
os0 f2 head f1 mainIn #connect
os0 f7 mainOut f16 tail #connect
os0 f16 head f54 mainIn #connect
os0 f6 mainOut f47 tail #connect
os0 f47 head f8 in #connect
os0 f48 mainOut f51 tail #connect
os0 f51 head f8 in #connect
os0 f17 mainOut f49 tail #connect
os0 f49 head f15 in #connect
os0 f11 mainOut f21 tail #connect
os0 f21 head f15 in #connect
os0 f39 mainOut f14 tail #connect
os0 f14 head f31 in #connect
os0 f34 mainOut f19 tail #connect
os0 f19 head f31 in #connect
