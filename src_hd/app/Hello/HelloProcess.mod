[Ivy]
17886960E38DFBA6 7.5.0 #module
>Proto >Proto Collection #zClass
Hs0 HelloProcess Big #zClass
Hs0 RD #cInfo
Hs0 #process
Hs0 @AnnotationInP-0n ai ai #zField
Hs0 @TextInP .type .type #zField
Hs0 @TextInP .processKind .processKind #zField
Hs0 @TextInP .xml .xml #zField
Hs0 @TextInP .responsibility .responsibility #zField
Hs0 @UdInit f0 '' #zField
Hs0 @UdProcessEnd f1 '' #zField
Hs0 @PushWFArc f2 '' #zField
Hs0 @UdEvent f3 '' #zField
Hs0 @UdExitEnd f4 '' #zField
Hs0 @PushWFArc f5 '' #zField
Hs0 @UdInit f6 '' #zField
Hs0 @UdProcessEnd f7 '' #zField
Hs0 @PushWFArc f8 '' #zField
>Proto Hs0 Hs0 HelloProcess #zField
Hs0 f0 guid 017886960E3F2C2B #txt
Hs0 f0 method startResult(String) #txt
Hs0 f0 inParameterDecl '<String name> param;' #txt
Hs0 f0 inParameterMapAction 'out.helloProcessData.isResult=true;
out.helloProcessData.name=param.name;
' #txt
Hs0 f0 outParameterDecl '<app.HelloProcessData helloProcessData> result;' #txt
Hs0 f0 outParameterMapAction 'result.helloProcessData=in.helloProcessData;
' #txt
Hs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>startResult(String)</name>
    </language>
</elementInfo>
' #txt
Hs0 f0 83 51 26 26 -16 15 #rect
Hs0 f0 @|UdInitIcon #fIcon
Hs0 f1 211 51 26 26 0 12 #rect
Hs0 f1 @|UdProcessEndIcon #fIcon
Hs0 f2 109 64 211 64 #arcP
Hs0 f3 guid 17886960E3FE768A #txt
Hs0 f3 actionTable 'out=in;
' #txt
Hs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Hs0 f3 83 147 26 26 -15 15 #rect
Hs0 f3 @|UdEventIcon #fIcon
Hs0 f4 211 147 26 26 0 12 #rect
Hs0 f4 @|UdExitEndIcon #fIcon
Hs0 f5 109 160 211 160 #arcP
Hs0 f6 guid 1788698368985166 #txt
Hs0 f6 method start() #txt
Hs0 f6 inParameterDecl '<> param;' #txt
Hs0 f6 inParameterMapAction 'out.helloProcessData.isResult=false;
' #txt
Hs0 f6 outParameterDecl '<String name,Boolean isResult> result;' #txt
Hs0 f6 outParameterMapAction 'result.name=in.helloProcessData.name;
result.isResult=in.helloProcessData.isResult;
' #txt
Hs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
    </language>
</elementInfo>
' #txt
Hs0 f6 67 243 26 26 -18 15 #rect
Hs0 f6 @|UdInitIcon #fIcon
Hs0 f7 211 243 26 26 0 12 #rect
Hs0 f7 @|UdProcessEndIcon #fIcon
Hs0 f8 93 256 211 256 #arcP
>Proto Hs0 .type app.Hello.HelloData #txt
>Proto Hs0 .processKind HTML_DIALOG #txt
>Proto Hs0 -8 -8 16 16 16 26 #rect
>Proto Hs0 '' #fIcon
Hs0 f0 mainOut f2 tail #connect
Hs0 f2 head f1 mainIn #connect
Hs0 f3 mainOut f5 tail #connect
Hs0 f5 head f4 mainIn #connect
Hs0 f6 mainOut f8 tail #connect
Hs0 f8 head f7 mainIn #connect
