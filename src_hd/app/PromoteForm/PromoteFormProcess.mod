[Ivy]
1783A9376C38DC87 7.5.0 #module
>Proto >Proto Collection #zClass
Ps0 PromoteFormProcess Big #zClass
Ps0 RD #cInfo
Ps0 #process
Ps0 @AnnotationInP-0n ai ai #zField
Ps0 @TextInP .type .type #zField
Ps0 @TextInP .processKind .processKind #zField
Ps0 @TextInP .xml .xml #zField
Ps0 @TextInP .responsibility .responsibility #zField
Ps0 @UdEvent f10 '' #zField
Ps0 @UdExitEnd f4 '' #zField
Ps0 @UdEvent f24 '' #zField
Ps0 @PushWFArc f27 '' #zField
Ps0 @Alternative f19 '' #zField
Ps0 @UdEvent f33 '' #zField
Ps0 @UdMethod f6 '' #zField
Ps0 @PushWFArc f44 '' #zField
Ps0 @GridStep f35 '' #zField
Ps0 @PushWFArc f5 '' #zField
Ps0 @UdEvent f3 '' #zField
Ps0 @PushWFArc f22 '' #zField
Ps0 @UdExitEnd f2 '' #zField
Ps0 @UdProcessEnd f38 '' #zField
Ps0 @PushWFArc f8 '' #zField
Ps0 @UdProcessEnd f16 '' #zField
Ps0 @UdProcessEnd f18 '' #zField
Ps0 @PushWFArc f17 '' #zField
Ps0 @GridStep f40 '' #zField
Ps0 @PushWFArc f39 '' #zField
Ps0 @GridStep f28 '' #zField
Ps0 @UdProcessEnd f43 '' #zField
Ps0 @UdProcessEnd f23 '' #zField
Ps0 @UdInit f0 '' #zField
Ps0 @PushWFArc f14 '' #zField
Ps0 @UdEvent f9 '' #zField
Ps0 @PushWFArc f45 '' #zField
Ps0 @GridStep f31 '' #zField
Ps0 @PushWFArc f12 '' #zField
Ps0 @GridStep f32 '' #zField
Ps0 @PushWFArc f42 '' #zField
Ps0 @PushWFArc f26 '' #zField
Ps0 @GridStep f30 '' #zField
Ps0 @UdProcessEnd f1 '' #zField
Ps0 @PushWFArc f11 '' #zField
Ps0 @UdEvent f13 '' #zField
Ps0 @PushWFArc f29 '' #zField
Ps0 @UdMethod f36 '' #zField
Ps0 @PushWFArc f21 '' #zField
Ps0 @UdProcessEnd f25 '' #zField
Ps0 @PushWFArc f20 '' #zField
Ps0 @UdInit f15 '' #zField
Ps0 @PushWFArc f34 '' #zField
Ps0 @UdProcessEnd f7 '' #zField
Ps0 @UdInit f47 '' #zField
Ps0 @GridStep f46 '' #zField
Ps0 @PushWFArc f48 '' #zField
Ps0 @PushWFArc f41 '' #zField
Ps0 @PushWFArc f37 '' #zField
>Proto Ps0 Ps0 PromoteFormProcess #zField
Ps0 f10 guid 1783E7DA99884631 #txt
Ps0 f10 actionTable 'out=in;
out.isCofirm=true;
' #txt
Ps0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isAdd</name>
    </language>
</elementInfo>
' #txt
Ps0 f10 739 51 26 26 -20 -38 #rect
Ps0 f10 @|UdEventIcon #fIcon
Ps0 f4 1355 467 26 26 0 12 #rect
Ps0 f4 @|UdExitEndIcon #fIcon
Ps0 f24 guid 1785831E57E54CAE #txt
Ps0 f24 actionTable 'out=in;
out.form.id=1;
' #txt
Ps0 f24 actionCode '
' #txt
Ps0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>SubmitForm</name>
    </language>
</elementInfo>
' #txt
Ps0 f24 1203 51 26 26 -17 -36 #rect
Ps0 f24 @|UdEventIcon #fIcon
Ps0 f27 1216 77 1216 242 #arcP
Ps0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Employees is empty ?</name>
    </language>
</elementInfo>
' #txt
Ps0 f19 1202 242 28 28 -116 -24 #rect
Ps0 f19 @|AlternativeIcon #fIcon
Ps0 f33 guid 178A5EE142318F98 #txt
Ps0 f33 actionTable 'out=in;
' #txt
Ps0 f33 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>BackToTaskList</name>
    </language>
</elementInfo>
' #txt
Ps0 f33 1549 53 22 22 14 0 #rect
Ps0 f33 @|UdEventIcon #fIcon
Ps0 f6 guid 1783E26733BDAAAC #txt
Ps0 f6 method preEdit(entity.Employee) #txt
Ps0 f6 inParameterDecl '<entity.Employee employee> param;' #txt
Ps0 f6 inParameterMapAction 'out.employee=param.employee;
out.headerDialog="Update information";
out.isCofirm=false;
' #txt
Ps0 f6 outParameterDecl '<> result;' #txt
Ps0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>preEdit(Employee)</name>
    </language>
</elementInfo>
' #txt
Ps0 f6 483 51 26 26 -56 -32 #rect
Ps0 f6 @|UdMethodIcon #fIcon
Ps0 f44 expr in #txt
Ps0 f44 outCond in.listEmployee.isEmpty() #txt
Ps0 f44 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Yes</name>
    </language>
</elementInfo>
' #txt
Ps0 f44 1216 270 1216 370 #arcP
Ps0 f44 0 0.21428571428571427 17 0 #arcLabel
Ps0 f35 actionTable 'out=in;
out.isCancel=false;
' #txt
Ps0 f35 actionCode 'import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

		FacesContext.getCurrentInstance().
                addMessage(null, 
                new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                									"List employee to promote is empty, add one or more and try again !","" ));


' #txt
Ps0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>update data&#13;
</name>
    </language>
</elementInfo>
' #txt
Ps0 f35 1160 370 112 44 -32 -16 #rect
Ps0 f35 @|StepIcon #fIcon
Ps0 f5 1368 77 1368 467 #arcP
Ps0 f3 guid 1783A9376CFEA2E2 #txt
Ps0 f3 actionTable 'out=in;
' #txt
Ps0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Ps0 f3 1355 51 26 26 -1 -30 #rect
Ps0 f3 @|UdEventIcon #fIcon
Ps0 f22 expr in #txt
Ps0 f22 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>No</name>
    </language>
</elementInfo>
' #txt
Ps0 f22 1202 256 1144 379 #arcP
Ps0 f22 1 1144 256 #addKink
Ps0 f22 1 0.23076923076923078 18 0 #arcLabel
Ps0 f2 1131 379 26 26 16 0 #rect
Ps0 f2 @|UdExitEndIcon #fIcon
Ps0 f38 1549 453 22 22 14 0 #rect
Ps0 f38 @|UdProcessEndIcon #fIcon
Ps0 f8 483 64 483 480 #arcP
Ps0 f8 1 456 64 #addKink
Ps0 f8 2 456 480 #addKink
Ps0 f8 1 0.5 0 0 #arcLabel
Ps0 f16 483 467 26 26 0 12 #rect
Ps0 f16 @|UdProcessEndIcon #fIcon
Ps0 f18 171 467 26 26 16 0 #rect
Ps0 f18 @|UdProcessEndIcon #fIcon
Ps0 f17 765 64 800 234 #arcP
Ps0 f17 1 800 64 #addKink
Ps0 f17 1 0.3451628523414815 0 0 #arcLabel
Ps0 f40 actionTable 'out=in;
' #txt
Ps0 f40 actionCode 'import entity.Employee;

Employee emp = new Employee();
emp.setName(in.employee.name);
emp.setAge(in.employee.age);
emp.setTeam(in.employee.team);

in.listEmployee.add(emp);

' #txt
Ps0 f40 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Add new employee</name>
    </language>
</elementInfo>
' #txt
Ps0 f40 744 234 112 44 -52 -8 #rect
Ps0 f40 @|StepIcon #fIcon
Ps0 f39 1560 340 1560 453 #arcP
Ps0 f28 actionTable 'out=in;
' #txt
Ps0 f28 actionCode 'import ch.ivy.addon.portal.generic.navigation.PortalNavigator;
import javax.faces.context.Flash;
import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;

FacesMessage message = new FacesMessage(ivy.cms.co("/Message/cancelTaskPromote"));
FacesContext.getCurrentInstance().addMessage("portal-global-growl-message", message);

Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
flash.put("overridePortalGrowl", true);
flash.setRedirect(true);
flash.setKeepMessages(true);

ivy.task.reset();

PortalNavigator portalNavigator = new PortalNavigator();
portalNavigator.navigateToPortalHome();' #txt
Ps0 f28 security system #txt
Ps0 f28 1542 316 36 24 20 -2 #rect
Ps0 f28 @|StepIcon #fIcon
Ps0 f43 955 467 26 26 0 12 #rect
Ps0 f43 @|UdProcessEndIcon #fIcon
Ps0 f23 739 467 26 26 0 12 #rect
Ps0 f23 @|UdProcessEndIcon #fIcon
Ps0 f0 guid 1783A9376C7404EF #txt
Ps0 f0 method start() #txt
Ps0 f0 inParameterDecl '<> param;' #txt
Ps0 f0 inParameterMapAction 'out.actualCurrentIndex=0;
out.form.comment="OK";
out.form.creator="Boss";
out.isApprove=false;
' #txt
Ps0 f0 inActionCode 'import entity.Employee;
import entity.Employee;
out.steps.add("Request");
out.steps.add("Response");

' #txt
Ps0 f0 outParameterDecl '<entity.PromoteForm promoteForm,List<entity.Employee> employees> result;' #txt
Ps0 f0 outParameterMapAction 'result.promoteForm=in.form;
result.employees=in.listEmployee;
' #txt
Ps0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
    </language>
</elementInfo>
' #txt
Ps0 f0 171 51 26 26 -5 -35 #rect
Ps0 f0 @|UdInitIcon #fIcon
Ps0 f14 696 77 739 480 #arcP
Ps0 f14 1 696 480 #addKink
Ps0 f14 0 0.5490029195171386 0 0 #arcLabel
Ps0 f9 guid 1783E740EA0B50F7 #txt
Ps0 f9 actionTable 'out=in;
out.employee=new entity.Employee();
out.headerDialog="Add new employee";
out.isCofirm=false;
' #txt
Ps0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>addNew</name>
    </language>
</elementInfo>
' #txt
Ps0 f9 683 51 26 26 -23 -38 #rect
Ps0 f9 @|UdEventIcon #fIcon
Ps0 f45 968 278 968 467 #arcP
Ps0 f45 0 0.5841402791975 0 0 #arcLabel
Ps0 f31 actionTable 'out=in;
' #txt
Ps0 f31 actionCode 'import javax.faces.application.FacesMessage;
import org.primefaces.context.RequestContext;
import javax.faces.context.FacesContext;
in.listEmployee.remove(in.employee);


FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success !", "Employee was deleted"));

RequestContext.getCurrentInstance().update("tableForm");
RequestContext.getCurrentInstance().update("growl");
RequestContext.getCurrentInstance().execute("PF(''employeeDialog'').hide()");

' #txt
Ps0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>remove employee</name>
    </language>
</elementInfo>
' #txt
Ps0 f31 912 234 112 44 -49 -8 #rect
Ps0 f31 @|StepIcon #fIcon
Ps0 f12 568 278 509 480 #arcP
Ps0 f12 1 568 480 #addKink
Ps0 f12 0 0.685793071189579 0 0 #arcLabel
Ps0 f32 actionTable 'out=in;
out.employee=in.employee;
' #txt
Ps0 f32 actionCode 'import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success !", "Information was updated"));

RequestContext.getCurrentInstance().update("tableForm");
RequestContext.getCurrentInstance().execute("PF(''employeeDialog'').hide()");
RequestContext.getCurrentInstance().update("growl");' #txt
Ps0 f32 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>update data</name>
    </language>
</elementInfo>
' #txt
Ps0 f32 512 234 112 44 -32 -8 #rect
Ps0 f32 @|StepIcon #fIcon
Ps0 f42 800 278 800 370 #arcP
Ps0 f42 0 0.13300492610837436 0 0 #arcLabel
Ps0 f26 800 414 765 480 #arcP
Ps0 f26 1 800 480 #addKink
Ps0 f26 0 0.7007356302749174 0 0 #arcLabel
Ps0 f30 actionTable 'out=in;
' #txt
Ps0 f30 actionCode 'import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success !", "Employee was added"));

RequestContext.getCurrentInstance().update("tableForm");
RequestContext.getCurrentInstance().update("growl");
RequestContext.getCurrentInstance().execute("PF(''employeeDialog'').hide()");' #txt
Ps0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>update data</name>
    </language>
</elementInfo>
' #txt
Ps0 f30 744 370 112 44 -32 -8 #rect
Ps0 f30 @|StepIcon #fIcon
Ps0 f1 171 275 26 26 0 12 #rect
Ps0 f1 @|UdProcessEndIcon #fIcon
Ps0 f11 568 77 568 234 #arcP
Ps0 f13 guid 1783F06280EAFE7F #txt
Ps0 f13 actionTable 'out=in;
out.isCofirm=true;
' #txt
Ps0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isUpdate</name>
    </language>
</elementInfo>
' #txt
Ps0 f13 555 51 26 26 -9 -33 #rect
Ps0 f13 @|UdEventIcon #fIcon
Ps0 f29 968 77 968 234 #arcP
Ps0 f36 guid 1785833A2A64723E #txt
Ps0 f36 method preDelete(entity.Employee) #txt
Ps0 f36 inParameterDecl '<entity.Employee employee> param;' #txt
Ps0 f36 inParameterMapAction 'out.employee=param.employee;
out.headerDialog="Delete employee";
out.isCofirm=false;
' #txt
Ps0 f36 outParameterDecl '<> result;' #txt
Ps0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>preDelete(Employee)</name>
    </language>
</elementInfo>
' #txt
Ps0 f36 955 51 26 26 -55 -32 #rect
Ps0 f36 @|UdMethodIcon #fIcon
Ps0 f21 1216 414 1216 469 #arcP
Ps0 f25 1205 469 22 22 14 0 #rect
Ps0 f25 @|UdProcessEndIcon #fIcon
Ps0 f20 184 373 184 467 #arcP
Ps0 f15 guid 17868A72791A1473 #txt
Ps0 f15 method startApproved(entity.PromoteForm,List<entity.Employee>) #txt
Ps0 f15 inParameterDecl '<entity.PromoteForm promoteForm,List<entity.Employee> employees> param;' #txt
Ps0 f15 inParameterMapAction 'out.actualCurrentIndex=1;
out.form=param.promoteForm;
out.isApprove=true;
out.isDetails=false;
out.listEmployee=param.employees;
' #txt
Ps0 f15 inActionCode 'out.steps.add("Request");
out.steps.add("Response");' #txt
Ps0 f15 outParameterDecl '<Boolean isCancel> result;' #txt
Ps0 f15 outParameterMapAction 'result.isCancel=in.isCancel;
' #txt
Ps0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>startApproved(PromoteForm,List&lt;Employee&gt;)</name>
    </language>
</elementInfo>
' #txt
Ps0 f15 171 347 26 26 -36 -33 #rect
Ps0 f15 @|UdInitIcon #fIcon
Ps0 f34 184 77 184 275 #arcP
Ps0 f7 189 701 22 22 14 0 #rect
Ps0 f7 @|UdProcessEndIcon #fIcon
Ps0 f47 guid 178B5A25FB08C74A #txt
Ps0 f47 method startDetails(entity.PromoteForm,List<entity.Employee>) #txt
Ps0 f47 inParameterDecl '<entity.PromoteForm promoteForm,List<entity.Employee> employees> param;' #txt
Ps0 f47 inParameterMapAction 'out.form=param.promoteForm;
out.isApprove=true;
out.isDetails=true;
out.listEmployee=param.employees;
' #txt
Ps0 f47 inActionCode 'import ch.ivyteam.ivy.workflow.ICase;
out.steps.add("Request");
out.steps.add("Response");
ICase case = ivy.wf.findCase(out.form.caseId.toNumber());
out.state = case.getState().name();
if(case.getState().name() == "DONE"){
out.actualCurrentIndex = 1;
}' #txt
Ps0 f47 outParameterDecl '<Boolean isCancel> result;' #txt
Ps0 f47 outParameterMapAction 'result.isCancel=in.isCancel;
' #txt
Ps0 f47 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>startDetails(PromoteForm,List&lt;Employee&gt;)</name>
    </language>
</elementInfo>
' #txt
Ps0 f47 188 572 24 24 -36 -33 #rect
Ps0 f47 @|UdInitIcon #fIcon
Ps0 f46 actionTable 'out=in;
' #txt
Ps0 f46 190 636 36 24 20 -2 #rect
Ps0 f46 @|StepIcon #fIcon
Ps0 f48 201 595 208 636 #arcP
Ps0 f41 208 660 201 701 #arcP
Ps0 f37 1560 75 1560 316 #arcP
>Proto Ps0 .type app.PromoteForm.PromoteFormData #txt
>Proto Ps0 .processKind HTML_DIALOG #txt
>Proto Ps0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel>Start</swimlaneLabel>
        <swimlaneLabel>Update Employee
</swimlaneLabel>
        <swimlaneLabel>Add new employee</swimlaneLabel>
        <swimlaneLabel>Delete Employee</swimlaneLabel>
        <swimlaneLabel>Submit form promote</swimlaneLabel>
        <swimlaneLabel>Close</swimlaneLabel>
        <swimlaneLabel>Back to task list</swimlaneLabel>
    </language>
    <swimlaneOrientation>true</swimlaneOrientation>
    <swimlaneSize>272</swimlaneSize>
    <swimlaneSize>240</swimlaneSize>
    <swimlaneSize>224</swimlaneSize>
    <swimlaneSize>232</swimlaneSize>
    <swimlaneSize>192</swimlaneSize>
    <swimlaneSize>192</swimlaneSize>
    <swimlaneSize>192</swimlaneSize>
    <swimlaneColor gradient="true">-10027009</swimlaneColor>
    <swimlaneColor gradient="true">-1</swimlaneColor>
    <swimlaneColor gradient="true">-10027009</swimlaneColor>
    <swimlaneColor gradient="true">-1</swimlaneColor>
    <swimlaneColor gradient="true">-10027009</swimlaneColor>
    <swimlaneColor gradient="true">-1</swimlaneColor>
    <swimlaneColor gradient="true">-10027009</swimlaneColor>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneType>LANE</swimlaneType>
    <swimlaneSpaceBefore>136</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
</elementInfo>
' #txt
>Proto Ps0 -8 -8 16 16 16 26 #rect
>Proto Ps0 '' #fIcon
Ps0 f30 mainOut f26 tail #connect
Ps0 f26 head f23 mainIn #connect
Ps0 f40 mainOut f42 tail #connect
Ps0 f42 head f30 mainIn #connect
Ps0 f31 mainOut f45 tail #connect
Ps0 f45 head f43 mainIn #connect
Ps0 f36 mainOut f29 tail #connect
Ps0 f29 head f31 mainIn #connect
Ps0 f15 mainOut f20 tail #connect
Ps0 f20 head f18 mainIn #connect
Ps0 f6 mainOut f8 tail #connect
Ps0 f8 head f16 mainIn #connect
Ps0 f13 mainOut f11 tail #connect
Ps0 f11 head f32 mainIn #connect
Ps0 f32 mainOut f12 tail #connect
Ps0 f12 head f16 mainIn #connect
Ps0 f9 mainOut f14 tail #connect
Ps0 f14 head f23 mainIn #connect
Ps0 f10 mainOut f17 tail #connect
Ps0 f17 head f40 mainIn #connect
Ps0 f3 mainOut f5 tail #connect
Ps0 f5 head f4 mainIn #connect
Ps0 f22 head f2 mainIn #connect
Ps0 f35 mainOut f21 tail #connect
Ps0 f21 head f25 mainIn #connect
Ps0 f24 mainOut f27 tail #connect
Ps0 f27 head f19 in #connect
Ps0 f28 mainOut f39 tail #connect
Ps0 f39 head f38 mainIn #connect
Ps0 f19 out f44 tail #connect
Ps0 f44 head f35 mainIn #connect
Ps0 f19 out f22 tail #connect
Ps0 f0 mainOut f34 tail #connect
Ps0 f34 head f1 mainIn #connect
Ps0 f47 mainOut f48 tail #connect
Ps0 f48 head f46 mainIn #connect
Ps0 f46 mainOut f41 tail #connect
Ps0 f41 head f7 mainIn #connect
Ps0 f33 mainOut f37 tail #connect
Ps0 f37 head f28 mainIn #connect
